export class ServiceItem {
  id: string;
  notes: string;
  plugin: string;
  selected: boolean;
  title: string;
}
