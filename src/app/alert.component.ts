import { Component } from '@angular/core';


import { OpenLPService } from './openlp.service';

@Component({
selector: 'openlp-remote-alert',
template: `
<h3>Send an Alert</h3>
<md-input-container>
  <input mdInput [(ngModel)]="alert" type="text" placeholder="Alert"></md-input-container>
<div>
<button md-raised-button color="warn" (click)="onSubmit()">Send</button>
</div>
`,
providers: [OpenLPService]
})

export class OpenLPAlertComponent {

  public alert: string;

  constructor(private openlpService: OpenLPService) { }

  onSubmit() {
    console.log('submitted: ', this.alert);
    this.openlpService.showAlert(this.alert);
    this.alert = '';
  }
}
