import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router'

import { OpenLPService } from './openlp.service';

@Component({
selector: 'openlp-remote-service',
template: `
<h3>Service items:</h3>
<div>
  <md-nav-list>
    <a md-list-item *ngFor="let item of items;let counter = index;" (click)="onItemSelected(counter)"  [class.selected]="item.selected">
      <p md-line>{{item.title}}<p>
    </a>
  </md-nav-list>
</div>
`,
providers: [OpenLPService]
})

export class OpenLPServiceComponent implements OnInit {
  items = null;
  ngOnInit() {
    this.getServiceItems();
  }

  onItemSelected(item) {
    this.openlpService.setServiceItem(item);
    this.router.navigate(['slides']);
  }

  getServiceItems() {
    this.openlpService.getServiceItems().then(items => this.items = items);
  }

  constructor(private openlpService: OpenLPService, private router: Router) {
    openlpService.stateChanged$.subscribe(item => this.getServiceItems());
  }

}
