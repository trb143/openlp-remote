import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MdSelectModule, MdButtonModule, MdInputModule, MdGridListModule, MdToolbarModule,
         MdListModule, MdSidenavModule, MdIconModule, MdIconRegistry } from '@angular/material';

import { AppComponent } from './app.component';
import { routing } from './app.routing';

import { OpenLPService } from './openlp.service';
import { OpenLPServiceComponent } from './service.component';
import { OpenLPSlidesComponent } from './slides.component';
import { OpenLPAlertComponent } from './alert.component';
import { OpenLPSearchComponent } from './search.component';


@NgModule({
  declarations: [
    AppComponent,
    OpenLPServiceComponent,
    OpenLPSearchComponent,
    OpenLPAlertComponent,
    OpenLPSlidesComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    BrowserAnimationsModule,
    MdListModule,
    MdSelectModule,
    MdSidenavModule,
    MdIconModule,
    MdToolbarModule,
    MdGridListModule,
    MdInputModule,
    MdButtonModule
  ],
  providers: [
    OpenLPService,
    MdIconRegistry
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
