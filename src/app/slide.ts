export class Slide {
	selected: boolean;
	html: string;
	tag: string;
	text: string;
	lines: string[];
}